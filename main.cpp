#include "getMat.hpp"
#include "../include/FlexArg.hpp"
#include "../include/FlexDefs.hpp"
#include <unistd.h>
#include "opencv2/opencv.hpp" 

using namespace cv;
using namespace mf;

#define EXPORT_METHOD extern "C" void *

typedef struct {
    // int cols;
    // int rows;
    int type;
    int channels;
    string inputValue;
} lib_param_t;

FLEX_ARG_TYPE static map<string, lib_param_t *> lib_param_map;

/***************************************
 * @brief global varaible must be static
 ***************************************/
// static int g_var = 0;

/**
 * @brief initial hook
 * 
 * @param param_tbl parameter list for initialization
 * @return EXPORT_METHOD 
 */
EXPORT_METHOD initial(string lib_id, param_tbl_t &param_tbl)
{
    if(lib_param_map.count(lib_id) == 0) {
        lib_param_t *lib_param_ptr = new lib_param_t;
        for(auto &e : param_tbl) {
            // if(e.param_key == "cols") {
            //     lib_param_ptr->cols = atoi(e.param_value.c_str());
            // } 
            // if(e.param_key == "rows") {
            //     lib_param_ptr->rows = atoi(e.param_value.c_str());
            // }
            if(e.param_key == "type") {
                if(e.param_value == "CV_8UC1") {
                    lib_param_ptr->type = CV_8UC1;
                } else if(e.param_value == "CV_8UC3") {
                    lib_param_ptr->type = CV_8UC3;
                }
            }
            if(e.param_key == "channels") {
                lib_param_ptr->channels = atoi(e.param_value.c_str());
            }
            if(e.param_key == "inputValue") {
                lib_param_ptr->inputValue = atoi(e.param_value.c_str());
            }
        }
        lib_param_map.insert(map<string, lib_param_t *>::value_type(lib_id, lib_param_ptr));
    }
    return nullptr;
}

/**
 * @brief 
 * 
 * @param input_arg_tbl arg list provided by prior stage 
 * @param output_arg_tbl arg list for following stage
 * @return EXPORT_METHOD 
 */
EXPORT_METHOD prepare(string lib_id, arg_tbl_t &input_arg_tbl, arg_tbl_t &output_arg_tbl)
{

    return nullptr;
}

/**
 * @brief work hook
 * 
 * @param input_arg_tbl arg list provided by prior stage 
 * @param output_arg_tbl arg list for following stage
 * @return EXPORT_METHOD 
 */
EXPORT_METHOD work(string lib_id, arg_tbl_t &input_arg_tbl, arg_tbl_t &output_arg_tbl)
{
    // use an input arg element by      e.use()
    // create a new arg element by      DEFINE_ARG(<arg_name>, <arg_length>>);
    // copy memory to an arg by         <arg_name>.assign(<some address>, <length>);
    // post an arg to output by         output_arg_tbl.push_back(<arg name>);
    lib_param_t *lib_param_ptr = lib_param_map[lib_id];
    if(lib_param_ptr != nullptr) {
        if (!checkInput(lib_param_ptr->inputValue, lib_param_ptr->channels)) {
            // 用户输入的字符串格式错误
            return nullptr;
        }

        if (lib_param_ptr->type==CV_8UC1) {
            int type_flag = 0;
        } else {
            int type_flag = 1;
        }

        Mat mat = getMat(lib_param_ptr->inputValue, type_flag);

        if(!mat.empty()) {
            DEFINE_ARG(img_arg,"DOUT",mat.cols * mat.rows * mat.channels());
            img_arg.assign(mat.data, mat.cols * mat.rows * mat.channels());
            output_arg_tbl.push_back(img_arg);

            
        }   
        
        // usleep(lib_param_ptr->interval * 1000);
    }
    return nullptr;
}

/**
 * @brief finish hook
 * 
 * @param input_arg_tbl 
 * @param output_arg_tbl 
 * @return EXPORT_METHOD 
 */
EXPORT_METHOD finish(string lib_id, arg_tbl_t &input_arg_tbl, arg_tbl_t &output_arg_tbl)
{

    return nullptr;
}

/**
 * @brief clean hook
 * 
 * @return EXPORT_METHOD 
 */
EXPORT_METHOD clean(string lib_id) 
{
    lib_param_t *lib_param_ptr = lib_param_map[lib_id];
    if(lib_param_ptr != nullptr) {
        delete lib_param_ptr;
        lib_param_ptr = nullptr;
        lib_param_map.erase(lib_id);
    }
    return nullptr;
}