#include <iostream>
#include <sstream>
#include <regex>
#include <string>
#include <set>
#include <opencv2/opencv.hpp>
#include <cstdlib>
#include <vector>
// using namespace std;
// using namespace cv;

//去除用户输入字符串中开头和结尾的中括号
void ridBrackets(string &s);

//检查用户输入的字符串能否构成一个方阵
bool checkSize(string s, int channels);

//检查用户输入的字符串的格式是否正确
bool checkInput(const string &s, int channels);

//得到用户输入的字符串所能构成的矩阵的行数和列数（已经保证用户输入的格式正确）
std::pair<int, int> getSize(const String &s);

//将用户输入的字符串转换成Mat格式的矩阵（已经保证用户输入的格式正确）
cv::Mat getMat(String &s, int type_flag);