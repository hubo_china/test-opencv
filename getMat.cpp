#include "getMat.hpp"

using namespace std;
using namespace cv;

//去除用户输入字符串中开头和结尾的中括号
void ridBrackets(string &s) {
    s.erase(0, 1);
    s.erase(s.end() - 1);
}


//检查用户输入的字符串能否构成一个方阵
bool checkSize(string s, int channels) {

    // channels必须是1或者3
    if (channels != 1 && channels != 3) {
        cout << "channels error! " << endl;
        return false;
    }

    ridBrackets(s);

    string str;
    string str_cin(s);
    stringstream ss;
    ss << str_cin;
 
    int row = 0;

    //用集合来存储每一行的列数（每一行的列数可能不一致）
    set<int> cols;

    //按照逗号和分号来划分行和列
    while (getline(ss, str, ';')) {
        ++row;  
        stringstream ss1;
        ss1 << str;
        string str1;
        int col = 0;
        while (getline(ss1, str1, ',')) {
            ++col;
        }
        cols.insert(col);
    }

    /*
    先检查各个行的列数是否一致，再根据通道数检查列数
    如果channels==1，检查列数和行数是否一致
    如果channels==3，检查列数是否能被3整除
    */ 
    if (cols.size() != 1) {
        return false;
    } else if (channels == 1 && row != *cols.begin()) {
        return false;
    } else if (channels == 3 && *cols.begin() % 3 != 0) {
        return false;
    } else {
        return true;
    }
}

//检查用户输入的字符串的格式是否正确
bool checkInput(const string &s, int channels) {
    cout << "checking " << s << "..." << endl;
    regex pattern("^\\[(([0-9]*\\.?[0-9]*,)+[0-9]*\\.?[0-9]*;)+([0-9]*\\.?[0-9]*,)+[0-9]*\\.?[0-9]*\\]$");
    
    // 先检查用户输入的字符串是否是"[x,x;x,x]"的格式（先不管行和列的匹配）
    if (!regex_match(s, pattern)) {
        cout << "Format error!" << endl;
        return false;
    } 

    // 检查行和列的匹配
    if (!checkSize(s, channels)) {
        cout << "Format error!" << endl;
        return false;
    }
    cout << "Format correct!" << endl;
    return true;
}

//得到用户输入的字符串所能构成的矩阵的行数和列数（已经保证用户输入的格式正确）
pair<int, int> getSize(const String &s) {
    string str;
    string str_cin(s);
    stringstream ss;
    ss << str_cin;
 
    int row = 0;
    int col = 0;

    while (getline(ss, str, ';')) {
        ++row;  
        stringstream ss1;
        ss1 << str;
        string str1;
        int col1 = 0;
        while (getline(ss1, str1, ',')) {
            ++col1;
        }
        col = col1;
    }
    return make_pair(row, col);
}

//将用户输入的字符串转换成Mat格式的矩阵（已经保证用户输入的格式正确）
Mat getMat(String &s, int type_flag) {
    ridBrackets(s);

    auto [row, col] = getSize(s);

    if (type_flag == 0) {
        Mat mat(row, col, CV_8UC1);
    } else {
        Mat mat(row, col, CV_8UC3);
    }

    string str;
    string str_cin(s);
    stringstream ss;
    ss << str_cin;

    int i = 0;

    while (getline(ss, str, ';')) {
        stringstream ss1;
        ss1 << str;
        string str1;
        int j = 0;
        while (getline(ss1, str1, ',')) {
            double f = atof(str1.c_str());
            mat.at<double>(i, j) = f;
            ++j;
        }
        ++i; 
    }
    return mat;
}

